using UnityEngine;

public class ItemBehaviour : MonoBehaviour
{
    [SerializeField]
    private Collider col;

    [SerializeField]
    private Material[] objMaterial;

    [SerializeField]
    private Renderer render;

    [SerializeField]
    private float yPlacementOffset = 0.5f;

    public bool canPlaceAtWall;
    public bool canPlaceAtGround;

    [HideInInspector]
    public bool isPickedUp;

    [HideInInspector]
    public bool overLap;

    private Rigidbody rbody;
    private Vector3 itemPos;

    private void Start()
    {
        itemPos = new Vector3(0, yPlacementOffset, 0);
        rbody = GetComponent<Rigidbody>();
    }

    public void ChangeItemMaterial(int state)
    {
        render.sharedMaterial = objMaterial[state];
    }

    public void SetParent(Transform parent, float rotationY = 0f)
    {
        transform.SetParent(parent);
        transform.localPosition = Vector3.zero;
        transform.localEulerAngles = new Vector3(0, rotationY, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Item"))
            overLap = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Item"))
            overLap = false;
    }

    public void SetPickedUp()
    {
        col.isTrigger = rbody.isKinematic = true;
    }

    public void SetPutDown(bool onWall, Vector3 throwPower)
    {
        col.isTrigger = false;
        rbody.isKinematic = onWall;
        rbody.useGravity = !onWall;
        rbody.AddForce(throwPower,ForceMode.Impulse);
    }

    public void MoveItem(float x, float z)
    {
        itemPos.x = x;
        itemPos.y = yPlacementOffset;
        itemPos.z = z;

        transform.position = itemPos;
        ChangeItemMaterial(overLap ? 2 : 1);
    }

    public void MoveItem(float x, float y, float z)
    {

        itemPos.x = x;
        itemPos.z = y;
        itemPos.y = z;
        //  print(itemPos);

        transform.position = itemPos;
        ChangeItemMaterial(overLap ? 2 : 1);
    }
}