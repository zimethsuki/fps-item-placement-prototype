using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Camera playerCam;
    public float mouseSensitivity = 100f;
    public bool lockCursor = true;

    [Space(10)]
    public Transform groundCheck;
    public float groundRadius = .4f;
    public LayerMask groundMask;

    [Space(10)]
    public float movementSpeed = 12f;
    public float jumpPower = 3f;
    public float gravity = -9.8f;

    private CharacterController controller;
    private bool isGrounded;
    private float xRotation = 0f;
    private Vector3 velocity = Vector3.zero;


    // Start is called before the first frame update
    private void Start()
    {
        controller = GetComponent<CharacterController>();
        Cursor.lockState = lockCursor ? CursorLockMode.Locked : CursorLockMode.None;
    }

    // Update is called once per frame
    private void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundRadius, groundMask);
        if(groundCheck && velocity.y < 0)
        {
            velocity.y = -1f;
        }
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.Rotate(Vector3.up * mouseX);
        playerCam.transform.localRotation = Quaternion.Euler(xRotation, 0, 0);

        float inputX = Input.GetAxis("Horizontal");
        float inputZ = Input.GetAxis("Vertical");

        Vector3 move = transform.right * inputX + transform.forward * inputZ;

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpPower * -2f * gravity);
        }

        velocity.y += gravity * Time.deltaTime;

        controller.Move(move * movementSpeed * Time.deltaTime);
        controller.Move(velocity * Time.deltaTime); 


    }
}