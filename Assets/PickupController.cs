using TMPro;
using UnityEngine;

public class PickupController : MonoBehaviour
{
    public Transform playerCamera;
    public ItemBehaviour currentItem;
    public Transform holdPosition;
    public TMP_Text infoText;

    public LayerMask itemLayer;
    public LayerMask buildAreaLayer;

    private LayerMask activeMask;

    private RaycastHit hit;
    public float throwPower = 100f;
    public bool isHoldingItem;
    public bool layerHit;
    public bool isOnWall;
    private bool itemInAreaMode;
    private bool itemFoundFlag;
    private bool itemLostFlag = true;

    private void Start()
    {
        activeMask = itemLayer;
    }

    private void FixedUpdate()
    {
        if (Physics.Raycast(playerCamera.position, playerCamera.TransformDirection(Vector3.forward), out hit, 1000, activeMask))
        {
            layerHit = true;
            Debug.DrawRay(playerCamera.position, playerCamera.TransformDirection(Vector3.forward) * 1000, Color.blue);

            if (isHoldingItem)
            {

                if (hit.transform.CompareTag("Ground Place Area") && currentItem.canPlaceAtGround)
                {
                    ItemPlacement(false);
                    currentItem.MoveItem(hit.point.x, hit.point.z);
                }
                else if (hit.transform.CompareTag("Wall Place Area") && currentItem.canPlaceAtWall)
                {
                    ItemPlacement(true);
                    currentItem.MoveItem(hit.point.x, hit.point.z, hit.point.y);
                }

            }
            else
            {
                ItemFound();
            }
        }
        else
        {
            Debug.DrawRay(playerCamera.position, playerCamera.TransformDirection(Vector3.forward) * 1000, Color.red);
            layerHit = false;

            if (isHoldingItem)
            {
                if (itemInAreaMode)
                {
                    isOnWall = false;
                    currentItem.ChangeItemMaterial(0);
                    currentItem.SetParent(holdPosition);
                    itemInAreaMode = false;

                }
            }
            else
            {
                ItemLost();
            }
        }
    }

    private void ItemFound()
    {
        if (!itemFoundFlag)
        {
            itemFoundFlag = true;
            itemLostFlag = false;
            infoText.text = "Press E to Pick Up Item";
        }
    }

    private void ItemLost()
    {
        if (!itemLostFlag)
        {
            itemFoundFlag = false;
            itemLostFlag = true;
            infoText.text = "";
        }
    }

    private void ItemPlacement(bool onWall)
    {

        if (!itemInAreaMode)
        {
            isOnWall = onWall;
            currentItem.SetParent(null, onWall ? 90f : 0f);
            itemInAreaMode = true;
        }

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
            if (isHoldingItem)
            {
                if (!currentItem.overLap)
                {
                    ItemLost();

                    currentItem.isPickedUp = false;
                    currentItem.ChangeItemMaterial(0);
                    activeMask = itemLayer;
                    isHoldingItem = false;
                    currentItem.transform.SetParent(null);
                    currentItem.SetPutDown(isOnWall, itemInAreaMode ? Vector3.zero : (playerCamera.forward + playerCamera.up) * throwPower);
                    currentItem = null;

                }
            }
            else
            {
                if (layerHit)
                {
                    isOnWall = false;
                    activeMask = buildAreaLayer;
                    isHoldingItem = true;
                    currentItem = hit.transform.GetComponent<ItemBehaviour>();
                    currentItem.SetPickedUp();
                    currentItem.SetParent(holdPosition);
                    infoText.text = "Press E to Put Down Item";

                    itemInAreaMode = false;
                }
            }

    }
}